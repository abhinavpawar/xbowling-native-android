package com.tribaltech.android.xbowling;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tribaltech.android.util.Data;

import rmn.androidscreenlibrary.ASSL;


public class Challenges extends Activity {
    Button webButton;
    String bowlingGameId;
    final String url = "http://www.xbowling.com/mobile/head2head.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenges);
        new ASSL(this, (ViewGroup) findViewById(R.id.root), 1134,
                720, false);
        webButton = (Button) findViewById(R.id.webButton);
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        bowlingGameId = Data.gameData.gameId;
        ((Button) findViewById(R.id.enterh2hPosted)).setText(Data.postedEntered ? "View" : "Enter");
        ((Button) findViewById(R.id.enterh2hLive)).setText(!Data.gameData.liveGameId.isEmpty() ? "View" : "Enter");
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.enterh2hLive: {
                Intent intent = new Intent(this, ChallengeView.class);
                startActivity(intent);
                finish();
                break;
            }

            case R.id.enterh2hPosted: {
                if (((Button) findViewById(R.id.enterh2hPosted)).getText().equals("Enter")) {
                    Intent intent = new Intent(this, ChooseOpponent.class);
                    intent.putExtra("bowlingGameId", bowlingGameId);
                    intent.putExtra("type", "posted");
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(this, H2HPostedMain.class);
                    startActivity(intent);
                    finish();
                }
//

//                Intent intent = new Intent(this, H2HPostedMain.class);
//                startActivity(intent);
//                finish();
                break;
            }

            case R.id.back: {
                Intent intent = new Intent(this, GameScreen.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, GameScreen.class);
        startActivity(intent);
        finish();
    }
}
