package com.tribaltech.android.xbowling;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tribaltech.android.util.CommonUtil;

public class webView extends Activity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        if (getIntent().hasExtra("url")) {

            webView = (WebView) findViewById(R.id.webView1);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setSupportZoom(true);
            webView.loadUrl(getIntent().getExtras().getString("url"));
            CommonUtil.loading_box(this, "Loading...");
            webView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    // do your stuff here
                    CommonUtil.loading_box_stop();
                }
            });

        } else {
            finish();
        }

    }
}