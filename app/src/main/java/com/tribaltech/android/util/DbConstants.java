package com.tribaltech.android.util;

public interface DbConstants {

    String DATABASE = "myDb";
    Integer DATABASE_VERSION = 9;
    String TABLE_NAME = "previous_games";
    String USER_NAME = "user_name";
    String CHECKOUT_ID = "checkout_id";
    String CENTER_TYPE = "center_type";
    String GAME_ID = "game_id";
    String LANE_NUMBER = "lane_number";
    String CENTER_NAME = "center_name";
    String LIVE_GAME_ID = "live_id";
    String SCREEN_NAME = "screen_name";
    String VENUE_ID = "venue_id";
    String PATTERN_NAME = "pattern_name";
    String PATTERN_LENGTH = "pattern_length";
    String COMP_TYPE = "comp_type";

}
